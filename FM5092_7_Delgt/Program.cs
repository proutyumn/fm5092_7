﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5092_7_Delgt
{
    class Program
    {
        public delegate double cdf(double a, double b);

        static void Main(string[] args)
        {
            Func<double, double> added = x => x + x;

            Console.WriteLine(added(5));
            Console.ReadLine();
        }

        public void MakeDelegateRefs()
        {
            cdf C = new cdf(normcdf);
            cdf anotherDeletgateRef = new cdf(anotherone);

            bool g = false;


            normcdf(g ? 5 : 3, 5);
        }

        public double normcdf(double a, double b)
        {
            return 0;
        }

        public double anotherone(double a, double b)
        {
            return 0;
        }
    }
}
